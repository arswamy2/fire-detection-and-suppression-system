#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/mcpwm_prelude.h"

#include "driver/gpio.h"

static const char *TAG2 = "test";

#define SERVO1_MIN_PULSEWIDTH_US 440  // Minimum pulse width in microsecond
#define SERVO1_MAX_PULSEWIDTH_US 2400  // Maximum pulse width in microsecond
#define SERVO1_MIN_DEGREE        -90   // Minimum angle
#define SERVO1_MAX_DEGREE        90    // Maximum angle
#define SERVO2_MIN_PULSEWIDTH_US 1500  // Minimum pulse width in microsecond
#define SERVO2_MAX_PULSEWIDTH_US 2475  // Maximum pulse width in microsecond
#define SERVO2_MIN_DEGREE        0   // Minimum angle
#define SERVO2_MAX_DEGREE        90    // Maximum angle

#define SERVO1_PULSE_GPIO             6        // GPIO connects to the PWM signal line
#define SERVO2_PULSE_GPIO             7   
#define SERVO_TIMEBASE_RESOLUTION_HZ 1000000  // 1MHz, 1us per tick
#define SERVO_TIMEBASE_PERIOD        20000    // 20000 ticks, 20ms

mcpwm_cmpr_handle_t comparator1 = NULL;
mcpwm_cmpr_handle_t comparator2 = NULL;

static inline uint32_t example_angle_to_compare(int angle, int servo_num)
{
    if(servo_num == 1)
        return (angle - SERVO1_MIN_DEGREE) * (SERVO1_MAX_PULSEWIDTH_US - SERVO1_MIN_PULSEWIDTH_US) / (SERVO1_MAX_DEGREE - SERVO1_MIN_DEGREE) + SERVO1_MIN_PULSEWIDTH_US;
    else if(servo_num==2)
        return (angle - SERVO2_MIN_DEGREE) * (SERVO2_MAX_PULSEWIDTH_US - SERVO2_MIN_PULSEWIDTH_US) / (SERVO2_MAX_DEGREE - SERVO2_MIN_DEGREE) + SERVO2_MIN_PULSEWIDTH_US;    
    return 0;
}

void servo_setup(){
    ESP_LOGI(TAG2, "Create timer and operator");
    mcpwm_timer_handle_t timer = NULL;
    mcpwm_timer_config_t timer_config = {
        .group_id = 0,
        .clk_src = MCPWM_TIMER_CLK_SRC_DEFAULT,
        .resolution_hz = SERVO_TIMEBASE_RESOLUTION_HZ,
        .period_ticks = SERVO_TIMEBASE_PERIOD,
        .count_mode = MCPWM_TIMER_COUNT_MODE_UP,
    };
    ESP_ERROR_CHECK(mcpwm_new_timer(&timer_config, &timer));

    mcpwm_oper_handle_t oper1 = NULL;
    mcpwm_operator_config_t operator_config1 = {
        .group_id = 0, // operator must be in the same group to the timer
    };
    ESP_ERROR_CHECK(mcpwm_new_operator(&operator_config1, &oper1));

    ESP_LOGI(TAG2, "Connect timer and operator");
    ESP_ERROR_CHECK(mcpwm_operator_connect_timer(oper1, timer));

    mcpwm_oper_handle_t oper2 = NULL;
    mcpwm_operator_config_t operator_config2 = {
        .group_id = 0, // operator must be in the same group to the timer
    };
    ESP_ERROR_CHECK(mcpwm_new_operator(&operator_config2, &oper2));

    ESP_LOGI(TAG2, "Connect timer and operator");
    ESP_ERROR_CHECK(mcpwm_operator_connect_timer(oper2, timer));

    ESP_LOGI(TAG2, "Create comparator and generator from the operator");
    
    mcpwm_comparator_config_t comparator_config1 = {
        .flags.update_cmp_on_tez = true,
    };
    ESP_ERROR_CHECK(mcpwm_new_comparator(oper1, &comparator_config1, &comparator1));


    mcpwm_comparator_config_t comparator_config2 = {
        .flags.update_cmp_on_tez = true,
    };
    ESP_ERROR_CHECK(mcpwm_new_comparator(oper2, &comparator_config2, &comparator2));

    mcpwm_gen_handle_t generator1 = NULL;
    mcpwm_generator_config_t generator_config1 = {
        .gen_gpio_num = SERVO1_PULSE_GPIO,
    };
    ESP_ERROR_CHECK(mcpwm_new_generator(oper1, &generator_config1, &generator1));

    mcpwm_gen_handle_t generator2 = NULL;
    mcpwm_generator_config_t generator_config2 = {
        .gen_gpio_num = SERVO2_PULSE_GPIO,
    };
    ESP_ERROR_CHECK(mcpwm_new_generator(oper2, &generator_config2, &generator2));


    // set the initial compare value, so that the servo will spin to the center position
    ESP_ERROR_CHECK(mcpwm_comparator_set_compare_value(comparator1, example_angle_to_compare(0,1)));
    ESP_ERROR_CHECK(mcpwm_comparator_set_compare_value(comparator2, example_angle_to_compare(0,2)));


    ESP_LOGI(TAG2, "Set generator action on timer and compare event");
    // go high on counter empty
    ESP_ERROR_CHECK(mcpwm_generator_set_action_on_timer_event(generator1,
                    MCPWM_GEN_TIMER_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, MCPWM_TIMER_EVENT_EMPTY, MCPWM_GEN_ACTION_HIGH)));
    ESP_ERROR_CHECK(mcpwm_generator_set_action_on_timer_event(generator2,
                    MCPWM_GEN_TIMER_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, MCPWM_TIMER_EVENT_EMPTY, MCPWM_GEN_ACTION_HIGH)));
    // go low on compare threshold
    ESP_ERROR_CHECK(mcpwm_generator_set_action_on_compare_event(generator1,
                    MCPWM_GEN_COMPARE_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, comparator1, MCPWM_GEN_ACTION_LOW)));
    ESP_ERROR_CHECK(mcpwm_generator_set_action_on_compare_event(generator2,
                    MCPWM_GEN_COMPARE_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, comparator2, MCPWM_GEN_ACTION_LOW)));

    ESP_LOGI(TAG2, "Enable and start timer");
    ESP_ERROR_CHECK(mcpwm_timer_enable(timer));
    ESP_ERROR_CHECK(mcpwm_timer_start_stop(timer, MCPWM_TIMER_START_NO_STOP));

    gpio_reset_pin(9);
    gpio_set_direction(9, GPIO_MODE_OUTPUT);

    gpio_reset_pin(35);
    gpio_set_direction(35,GPIO_MODE_OUTPUT);
    gpio_set_level(35,false);

}

void set_angles(int angle1, int angle2){
    if(angle1>90 || angle1<-90 || angle2>120 || angle2<0)
        return;
    ESP_ERROR_CHECK(mcpwm_comparator_set_compare_value(comparator1, example_angle_to_compare(angle1,1)));
    ESP_ERROR_CHECK(mcpwm_comparator_set_compare_value(comparator2, example_angle_to_compare(angle2,2)));
    vTaskDelay(pdMS_TO_TICKS(800));
}

void suppression_activate(int direction){ //2 is right, 1 is left

    gpio_set_level(35,true);
    
    if(direction == 2){
        set_angles(-90,52); //right edge
        gpio_set_level(9, true);
    }
    else if(direction == 1){
        set_angles(90,54); //left edge
        gpio_set_level(9, true);
    }
    
}

void suppression_deactivate(){
    gpio_set_level(9,false);
    gpio_set_level(35,false);
}

