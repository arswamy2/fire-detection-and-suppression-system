#include "mlx90614.h"

static i2c_port_t mlx90614_i2c_port;

void mlx90614_init(i2c_port_t i2c_port)
{
    mlx90614_i2c_port = i2c_port;
}

float mlx90614_read_temperature()
{
    uint8_t buffer[3];
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (MLX90614_I2C_ADDR << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, 0x07, true); // RAM address for object temperature
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (MLX90614_I2C_ADDR << 1) | I2C_MASTER_READ, true);
    i2c_master_read_byte(cmd, &buffer[0], I2C_MASTER_ACK);
    i2c_master_read_byte(cmd, &buffer[1], I2C_MASTER_ACK);
    i2c_master_read_byte(cmd, &buffer[2], I2C_MASTER_NACK);
    i2c_master_stop(cmd);

    i2c_master_cmd_begin(mlx90614_i2c_port, cmd, 1000 / portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);

    // Calculate temperature from received data
    uint16_t raw_data = (buffer[1] << 8) | buffer[0];
    float temperature = raw_data * 0.02 - 273.15;

    return temperature;
}
