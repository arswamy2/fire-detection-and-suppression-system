#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/i2c.h"
#include "mlx90614.h"
#include "esp_log.h"
#include "dual_servo.c"
#include "speaker.c"

#define I2C_SCL_GPIO 38
#define I2C_SDA_GPIO 39
#define I2C_SCL_GPIO_2 5
#define I2C_SDA_GPIO_2 4

#define NUM_READINGS_AVERAGE 3  

float BURNER_ON_THRESHOLD = 10;

static const char* TAG = "MyModule";
// I2C parameters for sensor 1
i2c_config_t i2c_config_1 = {
    .mode = I2C_MODE_MASTER,
    .sda_io_num = I2C_SDA_GPIO,
    //.sda_pullup_en = GPIO_PULLUP_ENABLE,
    .scl_io_num = I2C_SCL_GPIO,
    //.scl_pullup_en = GPIO_PULLUP_ENABLE,
    .master.clk_speed = 100000 // 100 kHz
};

// I2C parameters for sensor 2
i2c_config_t i2c_config_2 = {
    .mode = I2C_MODE_MASTER,
    .sda_io_num = I2C_SDA_GPIO_2,
    //.sda_pullup_en = GPIO_PULLUP_ENABLE,
    .scl_io_num = I2C_SCL_GPIO_2,
    //.scl_pullup_en = GPIO_PULLUP_ENABLE,
    .master.clk_speed = 100000 // 100 kHz
};

void measure_temp(float* average_temperature_1, float* average_temperature_2){
    mlx90614_init(I2C_NUM_0);
    // Average multiple readings for sensor 1 (left burner)
    *average_temperature_1 = mlx90614_read_temperature();
    for (int i = 0; i < NUM_READINGS_AVERAGE; ++i)
    {
        *average_temperature_1 += mlx90614_read_temperature();
    }
    *average_temperature_1 /= NUM_READINGS_AVERAGE;
    ESP_LOGI(TAG,"BURNER 1 is measuring %f\n",*average_temperature_1);

    // Average multiple readings for sensor 2 (right burner)
    mlx90614_init(I2C_NUM_1);  // Switch to the second I2C bus
    *average_temperature_2 = mlx90614_read_temperature();
    for (int i = 0; i < NUM_READINGS_AVERAGE; ++i)
    {
        *average_temperature_2 += mlx90614_read_temperature();
    }
    *average_temperature_2 /= NUM_READINGS_AVERAGE;
    ESP_LOGI(TAG,"BURNER 2 is measuring %f\n",*average_temperature_2);
}

void app_main(){
    servo_setup();
    example_ledc_init();
    // I2C driver for sensor 1
    i2c_param_config(I2C_NUM_0, &i2c_config_1);
    i2c_driver_install(I2C_NUM_0, i2c_config_1.mode, 0, 0, 0);

    // I2C driver for sensor 2
    i2c_param_config(I2C_NUM_1, &i2c_config_2);
    i2c_driver_install(I2C_NUM_1, i2c_config_2.mode, 0, 0, 0);

    // Initialize sensors
    mlx90614_init(I2C_NUM_0);
    float average_temperature_1 = 0;
    mlx90614_init(I2C_NUM_1);
    float average_temperature_2 = 0;
    bool burner_1_on = false;
    bool burner_2_on = false;

    float prev1 = 0;
    float prev2 = 0;

    bool prevon1 = false;
    bool prevon2 = false;
    gpio_reset_pin(18);
    gpio_set_direction(18,GPIO_MODE_INPUT);

    measure_temp(&average_temperature_1,&average_temperature_2); //initial reading to get a baseline
    BURNER_ON_THRESHOLD += (average_temperature_1+average_temperature_2)/2;
    ESP_LOGI(TAG,"threshold is %f",BURNER_ON_THRESHOLD);


    while (1)
    {
        prev1 = average_temperature_1;
        prev2 = average_temperature_2;
        measure_temp(&average_temperature_1,&average_temperature_2);

        if((average_temperature_1 - prev1)>30){
            ESP_LOGI(TAG,"Fire in burner 1?");
            fire_sound();
            vTaskDelay(pdMS_TO_TICKS(1000));
            suppression_activate(1);
        }
        else if ((average_temperature_2 - prev2)>30){
            ESP_LOGI(TAG,"Fire in burner 2?");
            fire_sound();
            vTaskDelay(pdMS_TO_TICKS(1000));
            suppression_activate(2);
        }
        

        prevon1 = burner_1_on;
        prevon2 = burner_2_on;

        // Determine burner status based on averaged temperature readings
        burner_1_on = burner_2_on ? (average_temperature_1 > (BURNER_ON_THRESHOLD+35)) : (average_temperature_1 > BURNER_ON_THRESHOLD);
        burner_2_on = burner_1_on ? (average_temperature_2 > BURNER_ON_THRESHOLD+35) : (average_temperature_2 > BURNER_ON_THRESHOLD);

        if(prevon1==false && burner_1_on==true)
            single_beep();
        if(prevon2==false && burner_2_on==true)
            double_beep();

        if(gpio_get_level(18)==0)
            end_fire_sound();
        
        if(burner_1_on) {
            ESP_LOGI(TAG,"BURNER 1 IS ON");
        }

        if(burner_2_on) {
            ESP_LOGI(TAG,"BURNER 2 IS ON\n");
        }

        // if(!burner_1_on && !burner_2_on){
        //     suppression_deactivate();
        // }

       
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
