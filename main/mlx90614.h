#ifndef MLX90614_H
#define MLX90614_H

#include "driver/i2c.h"

#define MLX90614_I2C_ADDR 0x5A

void mlx90614_init(i2c_port_t i2c_port);
float mlx90614_read_temperature();

#endif // MLX90614_H
